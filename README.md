
CẤU TRÚC THƯ MỤC
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources


Làm theo thứ tự từ mục (I) đến mục (III)

I. CÀI ĐẶT
-------

### Cài đặt các gói thư viện thông qua composer

Sử dụng câu lệnh dưới đây để cài đặt:

~~~
composer update
~~~


II. CẤU HÌNH
--------

### Database

Sửa file `config/db.php` với dữ liệu của bạn, ví dụ:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2basic',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```
III. CÀI ĐẶT CHƯƠNG TRÌNH
--------------------

### Bước 1: 
Chạy câu lệnh sau: 
~~~
yii migrate --migrationPath=@mdm/admin/migrations
~~~

### Bước 2: 
Chạy câu lệnh sau:
~~~
yii migrate --migrationPath=@yii/rbac/migrations
~~~ 

### Bước 3: 
Chạy câu lệnh sau: 
~~~
yii migrate
~~~ 