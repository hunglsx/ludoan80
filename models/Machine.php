<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\FileHelper;
use yii\imagine\Image;
/**
 * This is the model class for table "machine".
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property string|null $image
 * @property int|null $type
 * @property string|null $version
 * @property int $status
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class Machine extends \yii\db\ActiveRecord
{
    public $mediaFile;
    const MEDIA_DIR = 'uploads/images/';
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'machine';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['type', 'status', 'created_at', 'updated_at'], 'integer'],
            [['name', 'image', 'version'], 'string', 'max' => 255],
            ['mediaFile', 'required', 'on' => 'create']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Mã máy',
            'name' => 'Tên máy',
            'description' => 'Mô tả',
            'image' => 'Ảnh',
            'type' => 'Loại máy',
            'version' => 'Phiên bản',
            'status' => 'Trạng thái',
            'mediaFile' => 'Ảnh',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
        ];
    }
    
    public function upload() {
        if ($this->validate('mediaFile')) {
            $dir = self::MEDIA_DIR.Yii::$app->user->identity->id;
            if(!file_exists($dir)) {
                FileHelper::createDirectory($dir);
            }
            $path = $dir.'/'.time() . '_' . Yii::$app->security->generateRandomString(5) . 
                    '_'. md5($this->mediaFile->baseName) . '.' . $this->mediaFile->extension;
            $this->mediaFile->saveAs($path);
            $this->image = '/'.$path;
            $this->createImageThumb();
            return true;
        } else {
            return false;
        }
    }
    
    public function createImageThumb($source = false) {
        if($source == false) {
            $source = $this->image;
        }
        $thumbName = $this->getThumbName($source);
        $coverThumbName = $this->getThumbName($source, '/cover_');
        
        $origin = Yii::getAlias('@webroot'.$source);
        $thumb = Yii::getAlias('@webroot'.$thumbName);
        $cover = Yii::getAlias('@webroot'.$coverThumbName);
        
        if(file_exists($origin)) {
            Image::thumbnail($origin, 300, 200)
                ->save($thumb, ['quality' => 70]);
            
            Image::thumbnail($origin, 171, 180)
                ->save($cover, ['quality' => 70]);
            return true;
        }
        return false;
    }
    
    public function getThumbName($source = false, $prefix='/thumb_') {
        if($source == false) {
            $source = $this->image;
        }
        $baseName = basename($source);
        $dirName = dirname($source);
        $thumbName = $prefix . $baseName;
        return $dirName.$thumbName;
    }
    
    public static function getList() {
        $models = self::find()->all();
        $data = [];
        if($models) {
            foreach($models as $model) {
                $data[$model->id] = $model->name;
            }
        }
        return $data;
    }
    
    public function getMachineDetails() {
        return $this->hasMany(MachineDetail::className(), ['machine_id' => 'id']);
    }
}
