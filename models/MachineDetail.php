<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\FileHelper;
use yii\imagine\Image;

/**
 * This is the model class for table "machine_detail".
 *
 * @property int $id
 * @property int $machine_id
 * @property string $name
 * @property string|null $description
 * @property string|null $image
 * @property string|null $position
 * @property int $status
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class MachineDetail extends \yii\db\ActiveRecord
{
    public $mediaFile;
    const MEDIA_DIR = 'uploads/images/';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'machine_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['machine_id', 'name'], 'required'],
            [['machine_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['description'], 'string'],
            [['name', 'image', 'position'], 'string', 'max' => 255],
            ['mediaFile', 'required', 'on' => 'create']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Mã chi tiết máy',
            'machine_id' => 'Mã máy',
            'name' => 'Tên chi tiết máy',
            'description' => 'Mô tả',
            'image' => 'Ảnh',
            'position' => 'Vị trí',
            'status' => 'Trạng thái',
            'mediaFile' => 'Ảnh',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
        ];
    }
    
    public function upload() {
        if ($this->validate('mediaFile')) {
            $dir = self::MEDIA_DIR.Yii::$app->user->identity->id . '_mcdetail';
            if(!file_exists($dir)) {
                FileHelper::createDirectory($dir);
            }
            $path = $dir.'/'.time() . '_' . Yii::$app->security->generateRandomString(5) . 
                    '_'. md5($this->mediaFile->baseName) . '.' . $this->mediaFile->extension;
            $this->mediaFile->saveAs($path);
            $this->image = '/'.$path;
            $this->createImageThumb();
            return true;
        } else {
            return false;
        }
    }
    
    public function createImageThumb($source = false) {
        if($source == false) {
            $source = $this->image;
        }
        $thumbName = $this->getThumbName($source);
        $coverThumbName = $this->getThumbName($source, '/cover_');
        
        $origin = Yii::getAlias('@webroot'.$source);
        $thumb = Yii::getAlias('@webroot'.$thumbName);
        $cover = Yii::getAlias('@webroot'.$coverThumbName);
        
        if(file_exists($origin)) {
            Image::thumbnail($origin, 300, 200)
                ->save($thumb, ['quality' => 70]);
            
            Image::thumbnail($origin, 171, 180)
                ->save($cover, ['quality' => 70]);
            return true;
        }
        return false;
    }
    
    public function getThumbName($source = false, $prefix='/thumb_') {
        if($source == false) {
            $source = $this->image;
        }
        $baseName = basename($source);
        $dirName = dirname($source);
        $thumbName = $prefix . $baseName;
        return $dirName.$thumbName;
    }
}
