<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MachineDetail */

$this->title = Yii::t('app', 'Thêm chi tiết máy');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Máy'), 'url' => ['/machine/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="machine-detail-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
