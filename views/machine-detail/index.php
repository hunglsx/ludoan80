<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\detail\DetailView;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\models\searchs\MachineDetailSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Machine Details');
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs("$(function() {
    $('.model-push').click(function(e) {
        e.preventDefault();
        $('#modal').modal('show').find('.modal-content')
        .html($(this).attr('data'));
    });
});", yii\web\View::POS_READY);

?>
<div class="machine-detail-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Machine Detail'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <?php 
    Modal::begin([
        'header' => '<h4 class="modal-title">Chi tiết máy</h4>',
        'id' => 'modal',
        'size' => Modal::SIZE_LARGE
    ]);
    
    Modal::end();
    ?>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'machine_id',
            'name',
            'description:ntext',
            'image',
            [
                'attribute' => 'Thử', 
                'format' => 'raw',
                'value' => function($model) {
                    $data = DetailView::widget([
                        'model' => $model,
                        'condensed' => true,
                        'hover' => true,
                        'mode' => DetailView::MODE_VIEW,
                        'panel' => [
                            'heading' => '<i class="glyphicon glyphicon-book"></i> Chi tiết # ' . $model->id,
                            'type' => DetailView::TYPE_INFO,
                        ],
                        'attributes' => [
                            [
                                'group' => true,
                                'label' => 'THÔNG TIN',
                                'rowOptions' => ['class'=>'info'],
                                'groupOptions'=>['class'=>'text-center']
                            ],
                            'name',
                            [
                                'attribute' => 'description',
                                'format' => 'raw',
                                'value' => '<span class="text-justify"><em>' . $model->description . '</em></span>',
                                'type' => DetailView::INPUT_TEXTAREA, 
                                'options' => ['rows'=>4]
                            ],
                            [
                                'attribute' => 'image',
                                'format' => 'raw',
                                'value' => '<img src="'.$model->image.'" alt="'.$model->name.'">'
                            ],
                        ]
                    ]);
                    return  Html::a(Yii::t('app', 'Chi tiết'), 
                            ['#'], 
                            ['class' => 'btn btn-success model-push', 'id' => 'popupModal-'.$model->id, 'data' => $data]);
                }
            ],
            
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
