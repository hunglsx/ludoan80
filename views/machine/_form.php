<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\MachineType;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\Machine */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="machine-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?php 
        $pluginOptions = ['maxFileSize' => 30000];
        echo $form->field($model, 'mediaFile')->widget(FileInput::classname(), [
            'options' => ['multiple' => false, 'accept' => 'image/*'],
            'pluginOptions' => $pluginOptions
        ]); 
    ?>

    <?= $form->field($model, 'type')->dropDownList(MachineType::getLabels(), ['prompt'=>'----Chọn loại----']) ?>

    <?= $form->field($model, 'version')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
