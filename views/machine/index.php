<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ListView;
/* @var $this yii\web\View */
/* @var $searchModel app\models\searchs\MachineSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Danh sách máy');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="machine-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?= Html::a(Yii::t('app', 'Thêm máy'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    
    <?php 
    echo ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_machine',
        'layout' => '<div class="row">
                        <div class="col-md-12">
                            {pager}{summary}
                        </div>
                    </div>
                    <div class="row">{items}</div>',
    ]);
    ?>
    
    <?php Pjax::end(); ?>

</div>
