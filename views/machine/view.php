<?php

use yii\helpers\Html;
//use yii\widgets\DetailView;
use yii\web\View;

use kartik\detail\DetailView;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model app\models\Machine */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dánh sách máy'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
$this->registerJsFile(
    '@web/js/shieldui-all.min.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
$this->registerCssFile("@web/css/all.min.css", [
    'depends' => [\yii\bootstrap\BootstrapAsset::className()],
]);
$this->registerCssFile("@web/css/machine-detail.css", [
    'depends' => [\yii\bootstrap\BootstrapAsset::className()],
]);


$this->registerJs(
    '$(".tabs_div").shieldTabs();',
    View::POS_READY
);

$this->registerJs("$(function() {
    $('.model-push').click(function(e) {
        e.preventDefault();
        $('#modal').modal('show').find('.modal-content')
        .html($(this).attr('data'));
    });
});", View::POS_READY);

?>

<p>
    <?= Html::a(Yii::t('app', 'Thêm chi tiết máy'), ['/machine-detail/create', 'machine_id' => $model->id], ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Sửa'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?= Html::a(Yii::t('app', 'Xóa'), ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
    ]) ?>
</p>
<?php 
    Modal::begin([
        'header' => '<h4 class="modal-title">Chi tiết máy</h4>',
        'id' => 'modal',
        'size' => Modal::SIZE_LARGE
    ]);
    
    Modal::end();
?>
<div class="machine-view">
    <div class="row" style="margin-right: 0px !important; margin-left: 0px !important">
        <div class="col-md-12">
            <div class="row" id="gradient">
                <div class="col-md-6">
                    <img src="<?= $model->image;?>" class="img-responsive">
                </div>
                <div class="col-md-6" id="overview">
                    <div class="row">
                        <div class="col-xs-6 col-md-6">
                            <ul class="pb-product-details-ul">
                                <li><h3><strong><?=$model->name;?></strong></h3></li>
                                <li><span class="fa fa-calendar"><?=$model->version;?></span></li>
                            </ul>
                        </div>
                        <div class="col-xs-3 col-md-3 text-center" id="hits">
                            <span class="fa fa-bar-chart">&nbsp;24%</span>
                            <p>22,010,155 HITS</p>
                        </div>
                        <div class="col-xs-3 col-md-3 text-center" id="fan">
                            <a href="#"><span class="fa fa-heart">&nbsp;1904</span></a>
                            <p>BECOME A FAN</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3 col-md-3 pb-product-details-fa">
                            <span class="fa fa-mobile fa-lg"></span>
                            <h3><strong>5.5"</strong></h3>
                            <p>1440x2560 pixels</p>
                        </div>
                        <div class="col-xs-3 col-md-3 pb-product-details-fa">
                            <span class="fa fa-camera fa-lg"></span>
                            <h3><strong>12 MP</strong></h3>
                            <p>2160p</p>
                        </div>
                        <div class="col-xs-3 col-md-3 pb-product-details-fa">
                            <span class="fa fa-microchip fa-lg"></span>
                            <h3><strong>4GB RAM</strong></h3>
                            <p>Exynos 8890</p>
                        </div>
                        <div class="col-xs-3 col-md-3 pb-product-details-fa">
                            <span class="fa fa-battery-4 fa-lg"></span>
                            <h3><strong>3600mAH</strong></h3>
                            <p>Li-ion</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="tabs_div">
                    <ul>
                        <li>Mô tả</li>
                        <li>Chi tiết</li>
                    </ul>
                    <div>
                        <table class="table">
                            <tbody>
                                <tr class="row">
                                    <td class="success col-md-3">Tính năng và hoạt động: </td>
                                    <td><?=$model->description?></td>
                                </tr>
                                <tr class="row">
                                    <td class="success col-md-3">Đời máy: </td>
                                    <td><?=$model->version?></td>
                                </tr>
                                <tr class="row" style="border-bottom: 1px solid #ddd;">
                                    <td class="success col-md-3">Loại máy: </td>
                                    <td><?= \app\models\MachineType::getLabels()[$model->type]?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div>
                        <table class="table">
                            <tbody>
                                <?php 
                                $i = 0;
                                $size = sizeof($model->machineDetails);
                                ?>
                                <?php foreach($model->machineDetails as $detail): ?>
                                    <?php $i ++;?>
                                    <tr class="row" style="<?php echo ($i == $size)?'border-bottom: 1px solid #ddd;':''?>" >
                                        <td class="success col-md-3"><?=$detail->name?>: </td>
                                        <td><?=$detail->description?></td>
                                        <?php 
                                        $data = DetailView::widget([
                                            'model' => $detail,
                                            'condensed' => true,
                                            'hover' => true,
                                            'mode' => DetailView::MODE_VIEW,
                                            'panel' => [
                                                'heading' => '<i class="glyphicon glyphicon-book"></i> Chi tiết # ' . $detail->id,
                                                'type' => DetailView::TYPE_INFO,
                                            ],
                                            'attributes' => [
                                                [
                                                    'group' => true,
                                                    'label' => 'THÔNG TIN',
                                                    'rowOptions' => ['class'=>'info'],
                                                    'groupOptions'=>['class'=>'text-center']
                                                ],
                                                'name',
                                                [
                                                    'attribute' => 'description',
                                                    'format' => 'raw',
                                                    'value' => '<span class="text-justify"><em>' . $detail->description . '</em></span>',
                                                    'type' => DetailView::INPUT_TEXTAREA, 
                                                    'options' => ['rows' => 4]
                                                ],
                                                [
                                                    'attribute' => 'image',
                                                    'format' => 'raw',
                                                    'value' => '<img src="'.$detail->image.'" alt="'.$detail->name.'">'
                                                ],
                                            ]
                                        ]);
                                        ?>
                                        <td col-md-1>
                                            <?php 
                                            echo Html::a(Yii::t('app', '<span class="glyphicon glyphicon-eye-open"></span>'), 
                                            ['#'], 
                                            ['class' => 'btn btn-success model-push', 'id' => 'popupModal-'.$detail->id, 'data' => $data]);
                                            ?>
                                        </td>
                                    </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
