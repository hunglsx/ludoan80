<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\searchs\MachineSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="machine-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1,
            'class' => 'form-inline'
        ],
    ]); ?>

    
    <?= $form->field($model, 'name')->textInput(['placeholder' => 'Nhập tên máy'])->label(false) ?>

    <?= $form->field($model, 'type')->dropDownList(\app\models\MachineType::getLabels(), ['prompt'=>'----Tất cả loại máy----']) ?>

    <div class="form-group" style="margin-bottom: 10px;">
        <?= Html::submitButton(Yii::t('app', 'Tìm kiếm'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Làm lại'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
