<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>
<div class="post" style="margin-top: 5px;">
    <div class="col-xs-6 col-md-3" id="<?php echo 'machine-item-'.$model->id;?>">
        <div class="thumbnail">
            <a href="/machine/view?id=<?=$model->id;?>" target="_blank" title="<?=$model->name;?>">
                <img src="<?=$model->getThumbName();?>" alt="<?=$model->name;?>">
            </a>
            <div class="caption">
                <p class="hls-caption" style="display: block; line-height: 18px; max-height: 36px; overflow: hidden;  height: 36px;"><?=$model->name;?></p>
                <p>
                    <a style="margin-bottom: 5px;" href="/machine/view?id=<?=$model->id;?>" 
                       style="margin-left: 5px;" title="Xem" class="btn btn-success" 
                       role="button"><span class="glyphicon glyphicon-eye-open"></span></a>
                       
                    <a style="margin-bottom: 5px;" href="/machine/update?id=<?=$model->id;?>" 
                       style="margin-left: 5px;" title="Sửa" class="btn btn-info" 
                       role="button"><span class="glyphicon glyphicon-edit"></span></a>
                    
                    <a style="margin-bottom: 5px; margin-left: 5px;"
                       class="btn btn-danger media-item-delete" 
                       title="Xóa" aria-label="Xóa" 
                       href="/machine/delete?id=<?=$model->id;?>"
                       data-media-id="<?=$model->id;?>"
                       data-confirm="Are you sure you want to delete this item?" 
                       data-method="post">
                        <span class="glyphicon glyphicon-trash"></span>
                    </a>
                </p>
            </div>
        </div>
    </div>
</div>